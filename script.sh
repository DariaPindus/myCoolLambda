ARCH="index.zip"
FUNC_NAME="myCoolLambda"

if npm run build ; then
	rm $ARCH
	zip -r $ARCH .
	aws lambda update-function-code --function-name $FUNC_NAME --zip-file fileb://$ARCH
	echo 'uploaded' 
else
	echo ''
fi
